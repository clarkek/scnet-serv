--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hbeat; Type: TABLE; Schema: public; Owner: schub
--

CREATE TABLE public.hbeat (
    lasttime timestamp with time zone NOT NULL,
    cmd_resp character varying,
    cmd_sent character varying,
    uptime character varying,
    stats json,
    sernum character varying NOT NULL
);


ALTER TABLE public.hbeat OWNER TO schub;

--
-- Name: hub; Type: TABLE; Schema: public; Owner: schub
--

CREATE TABLE public.hub (
    sernum character varying NOT NULL,
    name character varying,
    location character varying,
    lasttime timestamp with time zone,
    command character varying,
    arg1 character varying,
    arg2 character varying,
    cmd_times integer,
    cmd_kill character varying,
    cmd_resp character varying,
    cmd_sent character varying,
    stats json
);


ALTER TABLE public.hub OWNER TO schub;

--
-- Name: sc; Type: TABLE; Schema: public; Owner: schub
--

CREATE TABLE public.sc (
    sernum character varying NOT NULL,
    name character varying,
    mac character varying
);

--
-- Name: config; Type: TABLE; Schema: public; Owner: schub
-- has config about default commands and last used common values
--

CREATE TABLE public.config (
    lasttime timestamp with time zone NOT NULL,
    command character varying, 
    arg1 character varying, 
    arg2 character varying,  
    cmd_times integer
    port integer
);

ALTER TABLE public.sc OWNER TO schub;

--
-- Name: hbeat hbeat_pkey; Type: CONSTRAINT; Schema: public; Owner: schub
--

ALTER TABLE ONLY public.hbeat
    ADD CONSTRAINT hbeat_pkey PRIMARY KEY (lasttime, sernum);


--
-- Name: hub hub_pkey; Type: CONSTRAINT; Schema: public; Owner: schub
--

ALTER TABLE ONLY public.hub
    ADD CONSTRAINT hub_pkey PRIMARY KEY (sernum);


--
-- Name: sc sc_pkey; Type: CONSTRAINT; Schema: public; Owner: schub
--

ALTER TABLE ONLY public.sc
    ADD CONSTRAINT sc_pkey PRIMARY KEY (sernum);

ALTER TABLE public.config OWNER TO schub ;
ALTER TABLE public.config  ADD CONSTRAINT config_pkey PRIMARY KEY (lasttime);

CREATE UNIQUE INDEX config_one_row ON config ((version IS NOT NULL ))
INSERT INTO  config VALUES ( current_timestamp ,'ps -aef' ,'', '', 25) ;

--
-- PostgreSQL database dump complete
--

