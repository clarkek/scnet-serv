#!/usr/bin/env python3
from flask import Flask, render_template, request, jsonify
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
import argparse
import re
import time
import json
import pdb

app = Flask(__name__)
app.config['DEBUG'] = True

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://schub:schub@127.0.0.1:5432/schub'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://schub:schub@127.0.0.1:5432/schub_dev'
db = SQLAlchemy(app)


parser = argparse.ArgumentParser(description='Manage avanto gateway and SC in the field')
parser.add_argument('-a', '--action', nargs=1,
                    default='status',
                    help='action to perform on hub status: prints status of hub; \
                            connect: ssh to nodes \
                            TODO hub:     Normal default, allow SC to sleep  \
                            TOOD nohub:   Keep SC awake.. ')
parser.add_argument('-s', '--serno', nargs=1,
                    default='b0c0code',
                    help='serno of hub default is unconnected sample b0c0code')
parser.add_argument('-p', '--psql', nargs=1,
                    default=['\dt+'],
                    help='NOT DONE psql command default is \dt+;')
parser.add_argument('-j', '--json', action='store_true',
                    default=False,
                    help='Print out json data of attached SC')
parser.add_argument('-d', '--debug', action='store_true',
                    default=False,
                    help='NOT DONE show data packets and other debug info')

args = parser.parse_args()
serno = str(args.serno[0])
action = str(args.action[0])


class config:
    command = 'ssh -R '
    arg1    = '19000:localhost:22'
    arg2    =  '-o StrictHostKeyChecking=no -N -vvv -i /home/pi/.ssh/boc_aws.pem '


    command = 'ps'
    arg1    = '-af'
    arg2    = '-ef'

    cmd_times =  25
    next_port =19001

class Hub(db.Model):
    sernum = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    location = db.Column(db.String ) 
    lasttime = db.Column(db.DateTime(timezone=True) ) 
    command = db.Column(db.String ) 
    arg1 = db.Column(db.String ) 
    arg2 = db.Column(db.String ) 
    cmd_times = db.Column(db.String)
    cmd_kill = db.Column(db.String)
    cmd_resp = db.Column(db.String)
    cmd_sent = db.Column(db.String)
    stats = db.Column(db.JSON) 

    def __repr__(self):
        return '<Hub %r>' % self.sernum

class Scpoll(db.Model):
    archives = db.Column(db.Boolean)
    welding  = db.Column(db.Boolean)
    battery  = db.Column(db.SmallInteger)
    temp     = db.Column(db.SmallInteger)
    sernum   = db.Column(db.String)
    btname   = db.Column(db.String)
    time     = db.Column(db.DateTime(timezone=True) , primary_key=True )
    mac      = db.Column(db.String, primary_key=True)

class Config(db.Model):
    lasttime = db.Column(db.DateTime(timezone=True) , primary_key=True) 
    command = db.Column(db.String ) 
    arg1 = db.Column(db.String ) 
    arg2 = db.Column(db.String ) 
    cmd_times = db.Column(db.String)
    port = db.Column(db.String)


cfg =  Config.query.one()
indb = Hub.query.filter_by(sernum = serno).first()
if not indb:
    print ('Hub with serno '  +serno + 'not found')
    sys.exit()
else:

    if args.json:
       pdb.set_trace()
       jstats= json.loads(indb.stats)
       scmacs = jstats.keys()

       print ('mac               btname               utcts               temp battery welding archives')
       for scmac in scmacs:
           scstr = '{btname:20} {utcts} {temp}   {battery}     {welding}    {archives}'.format(**jstats[scmac])
           print ( scmac, scstr  )

           scp = Scpoll.query.filter_by(mac=scmac, time=jstats[scmac]['utcts']).first()
           if scp:
               print ("Dup found, no commit")
           else:
               scp = Scpoll (archives = jstats[scmac]['archives']  , welding = jstats[scmac]['welding']  , battery  = jstats[scmac]['battery']  , temp = jstats[scmac]['temp']  , sernum = 'notknown'  , btname = jstats[scmac]['btname']  ,    time     = jstats[scmac]['utcts'] , mac = scmac )              

               db.session.add(scp)

    db.session.commit()


    if action == 'connect':
        # serv will take care of port count increment, not needed here
        indb.command = 'ssh -R '
#        cfg.port = cfg.port + 1 
        indb.arg1 = re.sub('19\d\d\d', str(cfg.port) ,'19000:localhost:22')
        indb.arg2 = '-o StrictHostKeyChecking=no -N -vvv -i /home/pi/.ssh/boc_aws.pem ubuntu@ec2-34-221-180-201.us-west-2.compute.amazonaws.com'
        indb.cmd_times = 1 
        indb.cmd_resp = '' 
        indb.lasttime = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    db.session.commit()

#    pdb.set_trace()
    while indb.cmd_resp == '':
        print ('waiting for hub to respond to command ')
        time.sleep (5)
        db.session.refresh(indb)
# TODO Have the ssh connection as first command write to a TMP file

    print ('hub has responded and is probably connected now copy and paste one of the below commands')
    print ('ssh pi@localhost -p '  + re.sub('.local.*','', indb.arg1))
    print ('scp  -P '  + re.sub('.local.*','', indb.arg1)   + ' pi@localhost:schub/scservice.log .   # DOWNLOAD latest scservice.log file')

