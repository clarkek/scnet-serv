from flask import Flask, render_template, request, jsonify
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
import os
import pdb
import re
import argparse
import json
app = Flask(__name__)
app.config['DEBUG'] = True

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://schub:schub@127.0.0.1:5432/schub'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://schub:schub@127.0.0.1:5432/schub_dev'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_CONNECTION_STRING')
db = SQLAlchemy(app)

class config:
    command = 'ssh -R -vv'
    arg1    = '19000:localhost:2040'
    arg2    =  '-i boc_aws.pem '

    command = 'ps'
    arg1    = '-af'
    arg2    = '-ef'

    cmd_times =  25
    next_port =19001

parser = argparse.ArgumentParser(description='Server for hub O&M, hearbeat and remote commands')
parser.add_argument('-a', '--addr',nargs=1,  
                    default=['0.0.0.0'],
                    help='Host address of interface to listen on ')

parser.add_argument('-p', '--port', nargs=1, 
                    default=['5000'],
                    help='Port to listen for heartbeats ')
args = parser.parse_args()

host = str(args.addr[0])
port = str(args.port[0])

class Hub(db.Model):
    sernum = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    location = db.Column(db.String ) 
    lasttime = db.Column(db.DateTime(timezone=True) ) 
    command = db.Column(db.String ) 
    arg1 = db.Column(db.String ) 
    arg2 = db.Column(db.String ) 
    cmd_times = db.Column(db.String)
    cmd_kill = db.Column(db.String)
    cmd_resp = db.Column(db.String)
    cmd_sent = db.Column(db.String)
    stats = db.Column(db.JSON) 
    
    def __repr__(self):
        return '<Hub %r>' % self.sernum

class Hbeat(db.Model):
    lasttime = db.Column(db.DateTime(timezone=True), primary_key=True ) 
    sernum = db.Column(db.String)
    cmd_resp = db.Column(db.String)
    cmd_sent = db.Column(db.String)
    uptime = db.Column(db.String)
    stats = db.Column(db.JSON) 
    
    def __repr__(self):
        return '<Hbeat %r>' % self.sernum


class Scpoll(db.Model):
    archives = db.Column(db.Boolean)
    welding  = db.Column(db.Boolean)
    battery  = db.Column(db.SmallInteger)
    temp     = db.Column(db.SmallInteger)
    sernum   = db.Column(db.String)
    btname   = db.Column(db.String)
    time     = db.Column(db.DateTime(timezone=True) , primary_key=True )
    mac      = db.Column(db.String, primary_key=True)


class SC(db.Model):
    sernum = db.Column(db.String, primary_key=True)
    mac= db.Column(db.String)
    name = db.Column(db.String)
    
    def __repr__(self):
        return '<SC %r>' % self.sernum

@app.route("/")
def hello_general():
      return "Hello  SC_NET Server "
      # return render_template('index.html');

@app.route("/scheart", methods=['GET','POST'])
def hello_heart():

    nextaction= {'command':'connect','arg1':'keepalive','arg2':'30'}
    serno = request.args.get('serno') 
    name =  request.args.get('name') 
    uptime =  request.args.get('uptime') 
    location =  request.args.get('location') 
  
    data = request.get_json()

    hbeat_time = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    indb = Hub.query.filter_by(sernum = serno).first()
    if not indb:

        indb = Hub(sernum = serno, name = name, location = location, lasttime = hbeat_time, 
                   command= config.command, arg1 = config.arg1, arg2 = config.arg2, cmd_times = config.cmd_times)
        db.session.add(indb)
    else:
        indb.lasttime = hbeat_time

    for key in data.keys():
        if key == 'command':
            indb.cmd_sent = data['command']
        if key == 'output':
            indb.cmd_resp = data['output']
        if key  == 'stats':
            indb.stats = data['stats']
            try:
                jstats = json.loads(data['stats'])
                scmacs = jstats.keys()
            except:
                scmacs = []
                print (' hbeat received from ' + serno + ' was invalid or non existing: '  + str(data['stats']) )
            else:
                print ('mac               btname               utcts               temp battery welding archives')
            #pdb.set_trace()

            for scmac in scmacs:
                scp = Scpoll.query.filter_by(mac=scmac, time=jstats[scmac]['utcts']).first()

                scstr = '{btname:20} {utcts} {temp}   {battery}     {welding}    {archives}'.format(**jstats[scmac])
                if scp:
                    print ( "DUP " , scmac, scstr  )
                else:
                    print ( "NEW " ,  scmac, scstr  )

                    scp = Scpoll (archives = jstats[scmac]['archives']  , welding = jstats[scmac]['welding']  , battery  = jstats[scmac]['battery']  , temp = jstats[scmac]['temp']  , sernum = 'notknown'  , btname = jstats[scmac]['btname']  ,    time     = jstats[scmac]['utcts'] , mac = scmac )              

                    db.session.add(scp)


    hbeatd = Hbeat (sernum=serno,lasttime=hbeat_time, 
                   cmd_sent=indb.cmd_sent,cmd_resp=indb.cmd_resp,stats=indb.stats,uptime=uptime)
    db.session.add(hbeatd)
    response  = {}
    response['serno'] = serno
    response['result'] = True
    nextaction = {}
    if indb.cmd_times:
        indb.cmd_times = indb.cmd_times - 1
        m =re.search( '^ssh -R',indb.command)
        if m:
            indb.arg1 = re.sub('19\d\d\d', str(config.next_port) ,indb.arg1)
            config.next_port = config.next_port + 1 
         
        nextaction['cmd_kill'] = indb.cmd_kill
        nextaction['command'] = indb.command
        nextaction['arg1'] = indb.arg1
        nextaction['arg2'] = indb.arg2
    data = [response, nextaction]
    db.session.commit()

    return jsonify(data)

@app.route("/scheartOLD", methods=['GET','POST'])
def hello_heart10():
    serno = request.args.get('serno') 
    return '''heartbeat resp at {} for sernum {}'''.format(datetime.now(), serno)

if (__name__ == "__main__"):
    app.run(host=host, port=port)

